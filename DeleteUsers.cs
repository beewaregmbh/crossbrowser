﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace CrossBrowser
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The DeleteUsers recording.
    /// </summary>
    [TestModule("31900307-abd3-4953-929a-e41ca87682b6", ModuleType.Recording, 1)]
    public partial class DeleteUsers : ITestModule
    {
        /// <summary>
        /// Holds an instance of the CrossBrowserRepository repository.
        /// </summary>
        public static CrossBrowserRepository repo = CrossBrowserRepository.Instance;

        static DeleteUsers instance = new DeleteUsers();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public DeleteUsers()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static DeleteUsers Instance
        {
            get { return instance; }
        }

#region Variables

        /// <summary>
        /// Gets or sets the value of variable Domain.
        /// </summary>
        [TestVariable("c2aaede6-0d98-4429-804c-bddaa0387466")]
        public string Domain
        {
            get { return repo.Domain; }
            set { repo.Domain = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "8.3")]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "8.3")]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SODALIS.User2Cross_EntryAdminCentral' at Center.", repo.SODALIS.User2Cross_EntryAdminCentralInfo, new RecordItemIndex(0));
            repo.SODALIS.User2Cross_EntryAdminCentral.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SODALIS.UsersDetailsComplexCardDeleteIcon' at Center.", repo.SODALIS.UsersDetailsComplexCardDeleteIconInfo, new RecordItemIndex(1));
            repo.SODALIS.UsersDetailsComplexCardDeleteIcon.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SODALIS.UsersDeleteConfirmationComponentButton' at Center.", repo.SODALIS.UsersDeleteConfirmationComponentButtonInfo, new RecordItemIndex(2));
            repo.SODALIS.UsersDeleteConfirmationComponentButton.Click();
            Delay.Milliseconds(200);
            
            try {
                Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating NotExists on item 'SODALIS.User2Cross_EntryAdminCentral'.", repo.SODALIS.User2Cross_EntryAdminCentralInfo, new RecordItemIndex(3));
                Validate.NotExists(repo.SODALIS.User2Cross_EntryAdminCentralInfo, null, false);
                Delay.Milliseconds(0);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(3)); }
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SODALIS.DeletedUsersTab' at Center.", repo.SODALIS.DeletedUsersTabInfo, new RecordItemIndex(4));
            repo.SODALIS.DeletedUsersTab.Click();
            Delay.Milliseconds(200);
            
            try {
                Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (InnerText='User2, Cross') on item 'SODALIS.DeletedUsersDetailsSimpleListItemTextN'.", repo.SODALIS.DeletedUsersDetailsSimpleListItemTextNInfo, new RecordItemIndex(5));
                Validate.AttributeEqual(repo.SODALIS.DeletedUsersDetailsSimpleListItemTextNInfo, "InnerText", "User2, Cross", null, false);
                Delay.Milliseconds(0);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(5)); }
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SODALIS.DeletedUsersDetailsSimpleListItemTextN' at Center.", repo.SODALIS.DeletedUsersDetailsSimpleListItemTextNInfo, new RecordItemIndex(6));
            repo.SODALIS.DeletedUsersDetailsSimpleListItemTextN.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SODALIS.DeletedUsersDetailsComplexCardRestoreI' at Center.", repo.SODALIS.DeletedUsersDetailsComplexCardRestoreIInfo, new RecordItemIndex(7));
            repo.SODALIS.DeletedUsersDetailsComplexCardRestoreI.Click();
            Delay.Milliseconds(200);
            
            try {
                Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating NotExists on item 'SODALIS.DeletedUsersDetailsSimpleListItemTextN'.", repo.SODALIS.DeletedUsersDetailsSimpleListItemTextNInfo, new RecordItemIndex(8));
                Validate.NotExists(repo.SODALIS.DeletedUsersDetailsSimpleListItemTextNInfo, null, false);
                Delay.Milliseconds(0);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(8)); }
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SODALIS.ActiveUsersTab' at Center.", repo.SODALIS.ActiveUsersTabInfo, new RecordItemIndex(9));
            repo.SODALIS.ActiveUsersTab.Click();
            Delay.Milliseconds(200);
            
            try {
                Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating Exists on item 'SODALIS.User2Cross_EntryAdminCentral'.", repo.SODALIS.User2Cross_EntryAdminCentralInfo, new RecordItemIndex(10));
                Validate.Exists(repo.SODALIS.User2Cross_EntryAdminCentralInfo, null, false);
                Delay.Milliseconds(0);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(10)); }
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
