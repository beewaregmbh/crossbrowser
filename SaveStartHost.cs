﻿/*
 * Created by Ranorex
 * User: StephanBast
 * Date: 05.07.2018
 * Time: 15:26
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace CrossBrowser
{
    /// <summary>
    /// Description of SaveStartHost.
    /// </summary>
    [TestModule("C7304035-B83F-4864-8482-CE1C6B37496C", ModuleType.UserCode, 1)]
    public class SaveStartHost : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public SaveStartHost()
        {
            // Do not delete - a parameterless constructor is required!
        }

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
             variables.start_endpoint = Host.CurrentHost;
           		 
        }
    }
}
