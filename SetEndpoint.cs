﻿/*
 * Created by Ranorex
 * User: StephanBast
 * Date: 05.07.2018
 * Time: 15:04
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace CrossBrowser
{
    /// <summary>
    /// Description of SetEndpoint.
    /// </summary>
    [TestModule("EF6C4B7B-C110-459A-987B-26059A9A9C66", ModuleType.UserCode, 1)]
    public class SetEndpoint : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public SetEndpoint()
        {
            // Do not delete - a parameterless constructor is required!
        }

string _browser = "chrome";
[TestVariable("55f859be-0e79-4f6c-b202-51f68668d213")]
public string browser
{
	get { return _browser; }
	set { _browser = value; }
}

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
           		
            
            if(browser=="safari")
            {
            	Host.MakeCurrentHost("Mac");
            }
            else
            {
            	Host.MakeCurrentHost(variables.start_endpoint);
            }
        }
    }
}
