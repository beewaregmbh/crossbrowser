﻿/*
 * Created by Ranorex
 * User: StephanBast
 * Date: 30.05.2018
 * Time: 12:38
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace CrossBrowser
{
    /// <summary>
    /// Ranorex user code collection. A collection is used to publish user code methods to the user code library.
    /// </summary>
    [UserCodeCollection]
    public class UserCodeCollection1
    {
        /// <summary>
    	/// This is a placeholder text. Please describe the purpose of the
    	/// user code method here. The method is published to the user code library
    	/// within a user code collection.
    	/// </summary>
    	[UserCodeMethod]
    	public static void TypeSpeed_On()
    	{
    		Mouse.DefaultMoveTime = 300;  
            Keyboard.DefaultKeyPressTime = 10;  
            Delay.SpeedFactor = 0.1;  
    	}
    	
    	[UserCodeMethod]
    	public static void TypeSpeed_Off()
    	{
    		Mouse.DefaultMoveTime = 300;  
            Keyboard.DefaultKeyPressTime = 100;  
            Delay.SpeedFactor = 1.0; 
    	}
    	
    	[UserCodeMethod]
    	public static void ClearTextInput(Ranorex.Adapter a)
		{
		   Report.Log(ReportLevel.Info,"Reset du contenu de " + a);
		   a.As<InputTag>().TagValue ="";
		}
    	
    	[UserCodeMethod]
    	public static void ClearTextTextarea(Ranorex.Adapter a)
		{
		   Report.Log(ReportLevel.Info,"Reset du contenu de " + a);
		   a.As<TextAreaTag>().TagValue ="";
		}
    }
}
