﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace CrossBrowser
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The copyWOrkplanDetail recording.
    /// </summary>
    [TestModule("cc4696cf-7ff7-4e54-90b2-8d55b608179d", ModuleType.Recording, 1)]
    public partial class copyWOrkplanDetail : ITestModule
    {
        /// <summary>
        /// Holds an instance of the CrossBrowserRepository repository.
        /// </summary>
        public static CrossBrowserRepository repo = CrossBrowserRepository.Instance;

        static copyWOrkplanDetail instance = new copyWOrkplanDetail();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public copyWOrkplanDetail()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static copyWOrkplanDetail Instance
        {
            get { return instance; }
        }

#region Variables

        /// <summary>
        /// Gets or sets the value of variable Domain.
        /// </summary>
        [TestVariable("c2aaede6-0d98-4429-804c-bddaa0387466")]
        public string Domain
        {
            get { return repo.Domain; }
            set { repo.Domain = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "8.3")]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "8.3")]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SODALIS.WorkplanName' at Center.", repo.SODALIS.WorkplanNameInfo, new RecordItemIndex(0));
            repo.SODALIS.WorkplanName.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SODALIS.WorkplanEditIcon' at Center.", repo.SODALIS.WorkplanEditIconInfo, new RecordItemIndex(1));
            repo.SODALIS.WorkplanEditIcon.Click();
            Delay.Milliseconds(200);
            
            try {
                Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating Exists on item 'SODALIS.ToolboxPanelWidgetBoxTaskboxListItem'.", repo.SODALIS.ToolboxPanelWidgetBoxTaskboxListItemInfo, new RecordItemIndex(2));
                Validate.Exists(repo.SODALIS.ToolboxPanelWidgetBoxTaskboxListItemInfo, null, false);
                Delay.Milliseconds(0);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(2)); }
            
            try {
                Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating Exists on item 'SODALIS.ToolboxPanelWidgetBoxToolboxListItem'.", repo.SODALIS.ToolboxPanelWidgetBoxToolboxListItemInfo, new RecordItemIndex(3));
                Validate.Exists(repo.SODALIS.ToolboxPanelWidgetBoxToolboxListItemInfo, null, false);
                Delay.Milliseconds(0);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(3)); }
            
            try {
                Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating Exists on item 'SODALIS.Copy_of_ToolboxPanelWidgetBoxToolboxListItem'.", repo.SODALIS.Copy_of_ToolboxPanelWidgetBoxToolboxListItemInfo, new RecordItemIndex(4));
                Validate.Exists(repo.SODALIS.Copy_of_ToolboxPanelWidgetBoxToolboxListItemInfo, null, false);
                Delay.Milliseconds(0);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(4)); }
            
            try {
                Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating Exists on item 'SODALIS.Copy_of_ToolboxPanelWidgetBoxToolboxListItem1'.", repo.SODALIS.Copy_of_ToolboxPanelWidgetBoxToolboxListItem1Info, new RecordItemIndex(5));
                Validate.Exists(repo.SODALIS.Copy_of_ToolboxPanelWidgetBoxToolboxListItem1Info, null, false);
                Delay.Milliseconds(0);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(5)); }
            
            try {
                Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating Exists on item 'SODALIS.Copy_of_ToolboxPanelWidgetBoxToolboxListItem2'.", repo.SODALIS.Copy_of_ToolboxPanelWidgetBoxToolboxListItem2Info, new RecordItemIndex(6));
                Validate.Exists(repo.SODALIS.Copy_of_ToolboxPanelWidgetBoxToolboxListItem2Info, null, false);
                Delay.Milliseconds(0);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(6)); }
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 1s.", new RecordItemIndex(7));
            Delay.Duration(1000, false);
            
            try {
                Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (InnerText='Replace battery of Dell E5570 - Copy 1') on item 'SODALIS.WorkplanContainerTitle'.", repo.SODALIS.WorkplanContainerTitleInfo, new RecordItemIndex(8));
                Validate.AttributeEqual(repo.SODALIS.WorkplanContainerTitleInfo, "InnerText", "Replace battery of Dell E5570 - Copy 1", null, false);
                Delay.Milliseconds(0);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(8)); }
            
            try {
                Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (InnerText='Replacing faulty battery with new one') on item 'SODALIS.WorkplanContainerDescription'.", repo.SODALIS.WorkplanContainerDescriptionInfo, new RecordItemIndex(9));
                Validate.AttributeEqual(repo.SODALIS.WorkplanContainerDescriptionInfo, "InnerText", "Replacing faulty battery with new one", null, false);
                Delay.Milliseconds(0);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(9)); }
            
            try {
                Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (TagValue='Replace battery of Dell E5570 - Copy 1') on item 'SODALIS.WorkplanContainerPropertiesName'.", repo.SODALIS.WorkplanContainerPropertiesNameInfo, new RecordItemIndex(10));
                Validate.AttributeEqual(repo.SODALIS.WorkplanContainerPropertiesNameInfo, "TagValue", "Replace battery of Dell E5570 - Copy 1", null, false);
                Delay.Milliseconds(0);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(10)); }
            
            try {
                Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (TagValue='Replacing faulty battery with new one') on item 'SODALIS.WorkplanContainerPropertiesDescription'.", repo.SODALIS.WorkplanContainerPropertiesDescriptionInfo, new RecordItemIndex(11));
                Validate.AttributeEqual(repo.SODALIS.WorkplanContainerPropertiesDescriptionInfo, "TagValue", "Replacing faulty battery with new one", null, false);
                Delay.Milliseconds(0);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(11)); }
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SODALIS.TaskContainerTaskName' at Center.", repo.SODALIS.TaskContainerTaskNameInfo, new RecordItemIndex(12));
            repo.SODALIS.TaskContainerTaskName.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (InnerText='Open laptop') on item 'SODALIS.TaskContainerTaskName'.", repo.SODALIS.TaskContainerTaskNameInfo, new RecordItemIndex(13));
            Validate.AttributeEqual(repo.SODALIS.TaskContainerTaskNameInfo, "InnerText", "Open laptop");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (TagValue='Open laptop') on item 'SODALIS.TaskContainerPropertiesInputName'.", repo.SODALIS.TaskContainerPropertiesInputNameInfo, new RecordItemIndex(14));
            Validate.AttributeEqual(repo.SODALIS.TaskContainerPropertiesInputNameInfo, "TagValue", "Open laptop");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (TagValue='Open laptop case by removing all 15 screws using philips head screw driver') on item 'SODALIS.TaskContainerPropertiesTextareaDescrip'.", repo.SODALIS.TaskContainerPropertiesTextareaDescripInfo, new RecordItemIndex(15));
            Validate.AttributeEqual(repo.SODALIS.TaskContainerPropertiesTextareaDescripInfo, "TagValue", "Open laptop case by removing all 15 screws using philips head screw driver");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (TagValue='5m') on item 'SODALIS.TaskContainerPropertiesInputExpectedDu'.", repo.SODALIS.TaskContainerPropertiesInputExpectedDuInfo, new RecordItemIndex(16));
            Validate.AttributeEqual(repo.SODALIS.TaskContainerPropertiesInputExpectedDuInfo, "TagValue", "5m");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeContains (InnerText>'0.png (7 kB)\n') on item 'SODALIS.TaskContainerPropertiesBoxFilesInstruc'.", repo.SODALIS.TaskContainerPropertiesBoxFilesInstrucInfo, new RecordItemIndex(17));
            Validate.AttributeContains(repo.SODALIS.TaskContainerPropertiesBoxFilesInstrucInfo, "InnerText", "0.png (7 kB)\n");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SODALIS.TaskContainerPropertiesBoxFilesInstruc' at Center.", repo.SODALIS.TaskContainerPropertiesBoxFilesInstrucInfo, new RecordItemIndex(18));
            repo.SODALIS.TaskContainerPropertiesBoxFilesInstruc.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating Exists on item 'SODALIS.ImgTag0Png'.", repo.SODALIS.ImgTag0PngInfo, new RecordItemIndex(19));
            Validate.Exists(repo.SODALIS.ImgTag0PngInfo);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeContains (InnerText>'0.png\n') on item 'SODALIS.DivTag0Png'.", repo.SODALIS.DivTag0PngInfo, new RecordItemIndex(20));
            Validate.AttributeContains(repo.SODALIS.DivTag0PngInfo, "InnerText", "0.png\n");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SODALIS.Close' at Center.", repo.SODALIS.CloseInfo, new RecordItemIndex(21));
            repo.SODALIS.Close.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SODALIS.CheckBox1' at Center.", repo.SODALIS.CheckBox1Info, new RecordItemIndex(22));
            repo.SODALIS.CheckBox1.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 1s.", new RecordItemIndex(23));
            Delay.Duration(1000, false);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (InnerText='CheckBox 1') on item 'SODALIS.CheckBox1'.", repo.SODALIS.CheckBox1Info, new RecordItemIndex(24));
            Validate.AttributeEqual(repo.SODALIS.CheckBox1Info, "InnerText", "CheckBox 1");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (TagValue='CheckBox 1') on item 'SODALIS.TextAreaTag2939a1f9Cc1b431489cbAb89b'.", repo.SODALIS.TextAreaTag2939a1f9Cc1b431489cbAb89bInfo, new RecordItemIndex(25));
            Validate.AttributeEqual(repo.SODALIS.TextAreaTag2939a1f9Cc1b431489cbAb89bInfo, "TagValue", "CheckBox 1");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SODALIS.Picture1' at Center.", repo.SODALIS.Picture1Info, new RecordItemIndex(26));
            repo.SODALIS.Picture1.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (InnerText='Picture 1') on item 'SODALIS.Picture1'.", repo.SODALIS.Picture1Info, new RecordItemIndex(27));
            Validate.AttributeEqual(repo.SODALIS.Picture1Info, "InnerText", "Picture 1");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (TagValue='Picture 1') on item 'SODALIS.TextAreaTag1a0ba8471eed4bf3A9ef796ad'.", repo.SODALIS.TextAreaTag1a0ba8471eed4bf3A9ef796adInfo, new RecordItemIndex(28));
            Validate.AttributeEqual(repo.SODALIS.TextAreaTag1a0ba8471eed4bf3A9ef796adInfo, "TagValue", "Picture 1");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (TagValue='Picture 1') on item 'SODALIS.TextAreaTag265eace28fc94e9cA25aB7844'.", repo.SODALIS.TextAreaTag265eace28fc94e9cA25aB7844Info, new RecordItemIndex(29));
            Validate.AttributeEqual(repo.SODALIS.TextAreaTag265eace28fc94e9cA25aB7844Info, "TagValue", "Picture 1");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating Exists on item 'SODALIS.ImagePropertiesPanelImage'.", repo.SODALIS.ImagePropertiesPanelImageInfo, new RecordItemIndex(30));
            Validate.Exists(repo.SODALIS.ImagePropertiesPanelImageInfo);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SODALIS.Text1' at Center.", repo.SODALIS.Text1Info, new RecordItemIndex(31));
            repo.SODALIS.Text1.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (InnerText='Text 1') on item 'SODALIS.Text1'.", repo.SODALIS.Text1Info, new RecordItemIndex(32));
            Validate.AttributeEqual(repo.SODALIS.Text1Info, "InnerText", "Text 1");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (TagValue='Text 1') on item 'SODALIS.TextAreaTag12da0251E8cf4ef9Bca81d2ce'.", repo.SODALIS.TextAreaTag12da0251E8cf4ef9Bca81d2ceInfo, new RecordItemIndex(33));
            Validate.AttributeEqual(repo.SODALIS.TextAreaTag12da0251E8cf4ef9Bca81d2ceInfo, "TagValue", "Text 1");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (TagValue='Text 1') on item 'SODALIS.TextAreaTag80792899130a4380B5eeB1942'.", repo.SODALIS.TextAreaTag80792899130a4380B5eeB1942Info, new RecordItemIndex(34));
            Validate.AttributeEqual(repo.SODALIS.TextAreaTag80792899130a4380B5eeB1942Info, "TagValue", "Text 1");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SODALIS.photo1' at Center.", repo.SODALIS.photo1Info, new RecordItemIndex(35));
            repo.SODALIS.photo1.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (InnerText='photo 1') on item 'SODALIS.photo1'.", repo.SODALIS.photo1Info, new RecordItemIndex(36));
            Validate.AttributeEqual(repo.SODALIS.photo1Info, "InnerText", "photo 1");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (TagValue='photo 1') on item 'SODALIS.TextAreaTag811431e108394bcaA56b91aab'.", repo.SODALIS.TextAreaTag811431e108394bcaA56b91aabInfo, new RecordItemIndex(37));
            Validate.AttributeEqual(repo.SODALIS.TextAreaTag811431e108394bcaA56b91aabInfo, "TagValue", "photo 1");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (TagValue='photo 1') on item 'SODALIS.TextAreaTag7c1039cbC46b43519184A43e4'.", repo.SODALIS.TextAreaTag7c1039cbC46b43519184A43e4Info, new RecordItemIndex(38));
            Validate.AttributeEqual(repo.SODALIS.TextAreaTag7c1039cbC46b43519184A43e4Info, "TagValue", "photo 1");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SODALIS.AddNewWorkplanButtonButtonIconAdd' at Center.", repo.SODALIS.AddNewWorkplanButtonButtonIconAddInfo, new RecordItemIndex(39));
            repo.SODALIS.AddNewWorkplanButtonButtonIconAdd.Click();
            Delay.Milliseconds(200);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
