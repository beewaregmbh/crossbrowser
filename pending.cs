﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace CrossBrowser
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The pending recording.
    /// </summary>
    [TestModule("89f42d21-ccd2-4791-b52e-7e7effd85ba3", ModuleType.Recording, 1)]
    public partial class pending : ITestModule
    {
        /// <summary>
        /// Holds an instance of the CrossBrowserRepository repository.
        /// </summary>
        public static CrossBrowserRepository repo = CrossBrowserRepository.Instance;

        static pending instance = new pending();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public pending()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static pending Instance
        {
            get { return instance; }
        }

#region Variables

        /// <summary>
        /// Gets or sets the value of variable Domain.
        /// </summary>
        [TestVariable("c2aaede6-0d98-4429-804c-bddaa0387466")]
        public string Domain
        {
            get { return repo.Domain; }
            set { repo.Domain = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "8.3")]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "8.3")]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SODALIS.PendingUsersTab' at Center.", repo.SODALIS.PendingUsersTabInfo, new RecordItemIndex(0));
            repo.SODALIS.PendingUsersTab.Click();
            Delay.Milliseconds(200);
            
            try {
                Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (InnerText=' asdad@asdad.cp') on item 'SODALIS.PendingInvitationsListItemEmail'.", repo.SODALIS.PendingInvitationsListItemEmailInfo, new RecordItemIndex(1));
                Validate.AttributeEqual(repo.SODALIS.PendingInvitationsListItemEmailInfo, "InnerText", " asdad@asdad.cp", null, false);
                Delay.Milliseconds(0);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(1)); }
            
            try {
                Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating Exists on item 'SODALIS.PendingInvitationsListItemImg'.", repo.SODALIS.PendingInvitationsListItemImgInfo, new RecordItemIndex(2));
                Validate.Exists(repo.SODALIS.PendingInvitationsListItemImgInfo, null, false);
                Delay.Milliseconds(0);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(2)); }
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
