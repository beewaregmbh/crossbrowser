﻿/*
 * Created by Ranorex
 * User: StephanBast
 * Date: 02.07.2018
 * Time: 15:01
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace CrossBrowser
{
    /// <summary>
    /// Description of test.
    /// </summary>
    [TestModule("6C7EAB82-4476-4CF0-A1A3-D93F9DF444BB", ModuleType.UserCode, 1)]
    public class test : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public test()
        {
            // Do not delete - a parameterless constructor is required!
        }

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
            
          	  String str = "animal";
		      int index = str.IndexOf("n");
		      Report.Info("log", index.ToString());
		      
   		
        }
    }
}
